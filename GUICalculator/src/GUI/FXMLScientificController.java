package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FXMLScientificController implements Initializable {

    @FXML
    private Button basicCalculator;
    @FXML
    private Button btnSeven;
    @FXML
    private TextField ScientificInputTextField;
    @FXML
    private TextField ScientificInputTextField2;
    @FXML
    private Button btnFour;
    @FXML
    private Button btnOne;
    @FXML
    private Button btnZero;
    @FXML
    private Button btnClear;
    @FXML
    private Button btnEight;
    @FXML
    private Button btnNine;
    @FXML
    private Button btnFive;
    @FXML
    private Button btnSix;
    @FXML
    private Button btnTwo;
    @FXML
    private Button btnThree;
    @FXML
    private Button btnSubtraction;
    @FXML
    private Button btnDot;
    @FXML
    private Button btnDivision;
    @FXML
    private Button btnMultiplication;
    @FXML
    private Button btnAddition;
    @FXML
    private Button btnEquals;
    @FXML
    private Button btnLeftBrace;
    @FXML
    private Button btnRightBrace;
    @FXML
    private Button sin;
    @FXML
    private Button cos;
    @FXML
    private Button tan;
    @FXML
    private Button elg;
    @FXML
    private Button lgt;


    @FXML
    void NavigateBasicCalculator(ActionEvent event) {
    	try {
			Parent basic_calculator_parent = FXMLLoader.load(getClass().getResource("FXMLBasic.fxml"));
			Scene basic_calculator_scene = new Scene(basic_calculator_parent, 800, 400);
			Stage stage = (Stage) (((Node) event.getSource()).getScene().getWindow());
			stage.hide();
			stage.setScene(basic_calculator_scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
    
    @FXML
    void OnActionBtnSeven(ActionEvent event) {
    	ScientificInputTextField.appendText("7");
    }
    
    @FXML
    void OnActionBtnFour(ActionEvent event) {
    	ScientificInputTextField.appendText("4");
    }
    
    @FXML
    void OnActionBtnAddition(ActionEvent event) {
    	ScientificInputTextField.appendText("+");
    }

    @FXML
    void OnActionBtnClear(ActionEvent event) {
    	ScientificInputTextField.clear();
    }

    @FXML
    void OnActionBtnDivision(ActionEvent event) {
    	ScientificInputTextField.appendText("/");
    }

    @FXML
    void OnActionBtnDot(ActionEvent event) {
    	ScientificInputTextField.appendText(".");
    }

    @FXML
    void OnActionBtnEight(ActionEvent event) {
    	ScientificInputTextField.appendText("8");
    }

    @FXML
    void OnActionBtnEquals(ActionEvent event) {
    	ScientificInputTextField2.appendText(ScientificInputTextField.getText());
    }

    @FXML
    void OnActionBtnFive(ActionEvent event) {
    	ScientificInputTextField.appendText("5");
    }

    @FXML
    void OnActionBtnMultiplication(ActionEvent event) {
    	ScientificInputTextField.appendText("x");
    }

    @FXML
    void OnActionBtnNine(ActionEvent event) {
    	ScientificInputTextField.appendText("9");
    }

    @FXML
    void OnActionBtnOne(ActionEvent event) {
    	ScientificInputTextField.appendText("1");
    }

    @FXML
    void OnActionBtnSix(ActionEvent event) {
    	ScientificInputTextField.appendText("6");
    }

    @FXML
    void OnActionBtnSubtraction(ActionEvent event) {
    	ScientificInputTextField.appendText("-");
    }

    @FXML
    void OnActionBtnThree(ActionEvent event) {
    	ScientificInputTextField.appendText("3");
    }

    @FXML
    void OnActionBtnTwo(ActionEvent event) {
    	ScientificInputTextField.appendText("2");
    }

    @FXML
    void OnActionBtnZero(ActionEvent event) {
    	ScientificInputTextField.appendText("0");
    }
    
    @FXML
    void OnActionBtnLeftBrace(ActionEvent event) {
    	ScientificInputTextField.appendText("(");
    }
    
    @FXML
    void OnActionBtnRightBrace(ActionEvent event) {
    	ScientificInputTextField.appendText(")");
    }
    
    @FXML
    void OnActionBtnSin(ActionEvent event) {
    	ScientificInputTextField.appendText("sin(");
    }
    
    @FXML
    void OnActionBtnCos(ActionEvent event) {
    	ScientificInputTextField.appendText("cos(");
    }
    
    @FXML
    void OnActionBtnTan(ActionEvent event) {
    	ScientificInputTextField.appendText("tan(");
    }
    
    @FXML
    void OnActionBtnNaturalLog(ActionEvent event) {
    	ScientificInputTextField.appendText("ln(");
    }
    
    @FXML
    void OnActionBtnLogTen(ActionEvent event) {
    	ScientificInputTextField.appendText("log(");
    }

}

